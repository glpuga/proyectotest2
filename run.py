from os.path import join, dirname
from vunit import VUnit


vu = VUnit.from_argv()

src_path = join(dirname(__file__), "src")

RC_lib = vu.add_library("RC_lib")
RC_lib.add_source_files(join(src_path, "*.vhd"))

RC_tb_lib = vu.add_library("tb_RC_lib")
RC_tb_lib.add_source_files(join(src_path, "test", "*.vhd"))


vu.main()
