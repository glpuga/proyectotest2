library vunit_lib;
context vunit_lib.vunit_context;

library ieee;
use ieee.std_logic_1164.all;

entity RC_tb is 
generic (runner_cfg : string := runner_cfg_default);
end RC_tb;

architecture arq of RC_tb is

component RC
port ( 
    A: in std_logic;
    B: in std_logic;
    Ci: in std_logic;
    Cout:out std_logic;
    Y : out std_logic
);
 end component;

signal A_tb , B_tb , Ci_tb , Cout_tb , Y_tb : std_logic;

begin 
    RC_1: RC port map (A_tb,B_tb,Ci_tb,Cout_tb,Y_tb);

main : process
begin

    test_runner_setup(runner, runner_cfg);

    wait for 10 ns;
    Ci_tb <= '0'; A_tb <='0'; B_tb <='0';
    check_equal(Y_tb, '0', "Comparando salida del sumador");
    check_equal(Cout_tb, '0', "Comparando carry de salida");
    wait for 10 ns; 
    Ci_tb <= '0'; A_tb <='0'; B_tb <='1';
    check_equal(Y_tb, '1', "Comparando salida del sumador");
    check_equal(Cout_tb, '0', "Comparando carry de salida");
    wait for 10 ns;
    Ci_tb <= '0'; A_tb <='1'; B_tb <='0';
    check_equal(Y_tb, '1', "Comparando salida del sumador");
    check_equal(Cout_tb, '0', "Comparando carry de salida");
    wait for 10 ns;
    Ci_tb <= '0'; A_tb <='1'; B_tb <='1';
    check_equal(Y_tb, '0', "Comparando salida del sumador");
    check_equal(Cout_tb, '1', "Comparando carry de salida");
    wait for 10 ns;
    Ci_tb <= '1'; A_tb <='0'; B_tb <='0';
    check_equal(Y_tb, '1', "Comparando salida del sumador");
    check_equal(Cout_tb, '0', "Comparando carry de salida");
    wait for 10 ns;
    Ci_tb <= '1'; A_tb <='0'; B_tb <='1';
    check_equal(Y_tb, '0', "Comparando salida del sumador");
    check_equal(Cout_tb, '1', "Comparando carry de salida");
    wait for 10 ns;
    Ci_tb <= '1'; A_tb <='1'; B_tb <='0';
    check_equal(Y_tb, '0', "Comparando salida del sumador");
    check_equal(Cout_tb, '1', "Comparando carry de salida");
    wait for 10 ns;
    Ci_tb <= '1'; A_tb <='1'; B_tb <='1';
    check_equal(Y_tb, '1', "Comparando salida del sumador");
    check_equal(Cout_tb, '1', "Comparando carry de salida");
    wait for 10 ns;

    test_runner_cleanup(runner);
end process;
end arq;
