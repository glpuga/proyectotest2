library ieee;
use ieee.std_logic_1164.all;

entity RC is 
port ( 
    A : in std_logic;
    B : in std_logic;
    Ci: in std_logic;
    Cout:out std_logic;
    Y : out std_logic
);
end RC;

architecture arq of RC is

begin 

    Y <= A xor B xor Ci;
    Cout <= (A and B) or (A and Ci) or (B and Ci);

end arq;
